# lexthink/logic

## Requirements

* PHP 7.0 or higher
* Composer dependency manager installed

## Installation

Install it by `git clone https://bitbucket.org/lexthink/logic.git` or download zip file.

```shell
$ git clone https://bitbucket.org/lexthink/logic.git
$ cd logic
$ composer install
```

## Basic usage

```shell
$ php bin/logic
```
Now, the command will ask you for the value of all the required arguments.

## PHP Support

This library is compatible with PHP versions 7.0 and above.
Please, try to use the full potential of the language.

## License

This library is licensed under [MIT license](http://opensource.org/licenses/MIT). Please see [LICENSE](LICENSE) for more information.